package com.open.free.boot.core.entity;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/13 10:21
 * @Description
 */
public class RequestPage {
    private Integer pageNo;
    private Integer pageSize;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
