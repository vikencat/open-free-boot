package com.open.free.boot.core.entity;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/11 23:42
 * @Description
 */
public class SmsEntity {
    //手机号码
    private String phone;
    // 验证码code
    private String code;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
