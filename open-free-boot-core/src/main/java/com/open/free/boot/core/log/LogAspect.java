package com.open.free.boot.core.log;


import com.open.free.boot.common.lang.DateTimeUtils;
import com.open.free.boot.common.lang.JsonUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/6 10:35
 * @Description 统一操作日志，记录所有请求请求参数
 */
@Aspect
@Component
public class LogAspect {
    private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);


    @Pointcut("execution(public * com.open.free.boot.web.controller..*.*(..))")
    public void pointLog(){

    }
    @Before("pointLog()")
    public void doMethodBefore(JoinPoint joinPoint){
        logger.info("执行日志切面：{}","doMethodBefore--方法执行前调用");
        Object[] obj = joinPoint.getArgs();


        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature)signature;

        Method method = methodSignature.getMethod();

        String methodName = method.getName();
        String className = signature.getDeclaringTypeName();

        String[] params2 = methodSignature.getParameterNames();
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // ajax json对象
        String params = null;
        if(request.getParameterMap().isEmpty()){
            params = JsonUtils.toJsonString(obj);
        }else{
            params = JsonUtils.toJsonString(request.getParameterMap());
        }
        String reqURI = request.getRequestURI();

        logger.info("-===============审计日志：路径:-={},参数-=:{},类-=:{},方法-=:{}",
                reqURI,params,className,methodName);

    }
}
