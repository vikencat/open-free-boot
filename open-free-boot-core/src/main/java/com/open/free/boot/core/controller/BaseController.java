package com.open.free.boot.core.controller;

import com.open.free.boot.core.result.ResultData;
import com.open.free.boot.core.result.ResultEnum;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/4 16:11
 * @Description
 */
public class BaseController<T> {
    /**
     * 操作成功返回
     * @return
     */
    public ResultData resultSuccess(){
        return  new ResultData<>(ResultEnum.HTTP_SUCCESS);
    }

    public ResultData resultSuccess(T data){
        return  new ResultData<>(ResultEnum.HTTP_SUCCESS,data);
    }
    /**
     * 返回失败信息
     * @param resultEnum
     * @return
     */
    public ResultData resultFail(ResultEnum resultEnum){
        return new ResultData<>(resultEnum);
    }




}
