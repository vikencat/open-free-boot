package com.open.free.boot.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.open.free.boot.web.mapper.SysAreaModelMapper;
import com.open.free.boot.web.mapper.SysCityModelMapper;
import com.open.free.boot.web.mapper.SysProvinceModelMapper;
import com.open.free.boot.web.mapper.customize.SysAreaModelCustomizeMapper;
import com.open.free.boot.web.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/12 22:33
 * @Description
 */
@Service
public class SysAreaService {
    @Autowired
    private SysProvinceModelMapper sysProvinceModelMapper;
    @Autowired
    private SysCityModelMapper sysCityModelMapper;
    @Autowired
    private SysAreaModelMapper areaModelMapper;
    @Autowired
    private SysAreaModelCustomizeMapper sysAreaModelCustomizeMapper;

    /**
     * 获取所有省区域数据
     * @param pageNo
     * @param pageSize
     * @return
     */
    public PageInfo<SysProvinceModel> getSysProvinceModelList(Integer pageNo,Integer pageSize){
        return  PageHelper.startPage(pageNo, pageSize).doSelectPageInfo(()->{
            sysProvinceModelMapper.selectByExample(null);
        });
    }

    /**
     * 查询当前省下的城市
     * @param provinceCode
     * @return
     */
    public List<SysCityModel> getSysCityModelMapperList(Integer provinceCode){
        SysCityModelExample example = new SysCityModelExample();
        example.createCriteria().andProvinceCodeEqualTo(provinceCode);
        return sysCityModelMapper.selectByExample(example);
    }

    /**
     * 查询当前城市下的区县
     * @param cityCode
     * @return
     */
    public List<SysAreaModel> getSysAreaModelMapperList(Integer cityCode){
        SysAreaModelExample example = new SysAreaModelExample();
        example.createCriteria().andCityCodeEqualTo(cityCode);
        return areaModelMapper.selectByExample(example);
    }

    /**
     * 停用省市区
     * @param id 省=1 市=2 区=3
     *
     */
    public void unEnableStatus(Integer id,Integer tableType,Integer status){
        Map<String,Object> params = new HashMap<>();
        params.put("id",id);
        params.put("tableType",tableType);
        params.put("status",status);
        sysAreaModelCustomizeMapper.updateEnableStatus(params);
    }

    /**
     * 停用省市区
     * @param id 省=1 市=2 区=3
     *
     */
    public void deleteArea(Integer id,Integer tableType){
        Map<String,Object> params = new HashMap<>();
        params.put("id",id);
        params.put("tableType",tableType);
        sysAreaModelCustomizeMapper.deleteArea(params);
    }
}
