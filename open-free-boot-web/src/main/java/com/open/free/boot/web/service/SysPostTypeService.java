package com.open.free.boot.web.service;

import com.open.free.boot.web.mapper.SysPostTypeMapper;
import com.open.free.boot.web.model.SysPostType;
import com.open.free.boot.web.model.SysPostTypeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Liuzhiwei
 * @date 2020/09/28
 */
@Service
public class SysPostTypeService {

    @Autowired
    private SysPostTypeMapper sysPostTypeMapper;

    public List<SysPostType> getEnableSysPostTypeList() {
        SysPostTypeExample example = new SysPostTypeExample();
        example.createCriteria().andEnableEqualTo(0);
        return sysPostTypeMapper.selectByExample(example);
    }

}
