package com.open.free.boot.web.service;

import com.open.free.boot.web.mapper.SysPostMapper;
import com.open.free.boot.web.model.SysPost;
import com.open.free.boot.web.model.SysPostExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Liuzhiwei
 * @date 2020/09/29
 * @version 1.0
 */
@Service
public class SysPostService {

    @Autowired
    private SysPostMapper sysPostMapper;

    public void insert(SysPost request) {
        sysPostMapper.insertSelective(request);
    }

    public void updateById(SysPost request) {
        sysPostMapper.updateByPrimaryKeySelective(request);
    }

    public void removeById(SysPost request) {
        sysPostMapper.deleteByPrimaryKey(request.getId());
    }

    public List<SysPost> selectByQuery() {
        SysPostExample example = new SysPostExample();
        return sysPostMapper.selectByExample(example);
    }
}
