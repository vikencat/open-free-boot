package com.open.free.boot.web.mapper.customize;

import com.open.free.boot.web.model.customize.OrganCustomize;

import java.util.List;
import java.util.Map;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/10/3 15:45
 * @Description
 */
public interface OrganOfficeCustomizeMapper {

    List<OrganCustomize> getOrganCustomizeByParentId(Map<String,Object> params);

    OrganCustomize getOrganizeById(Map<String,Object> params);

}
