package com.open.free.boot.web.mapper.customize;

import java.util.Map;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/13 0:14
 * @Description
 */
public interface SysAreaModelCustomizeMapper {
    /**
     * 省市区开停使用
     * @param params
     */
     void updateEnableStatus(Map<String,Object> params);

    /**
     * 删除省市区
     * @param params
     */
     void deleteArea(Map<String,Object> params);
}
