package com.open.free.boot.web.requestQuery;

import com.open.free.boot.core.entity.RequestPage;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/13 21:58
 * @Description
 */
public class UserRequest extends RequestPage {
    private String userName;
    private String userMobile;
    private String passWord;
    private String code;
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
