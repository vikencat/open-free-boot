package com.open.free.boot.web.controller;

import com.open.free.boot.core.annotation.JwtIgnore;
import com.open.free.boot.core.controller.BaseController;
import com.open.free.boot.core.result.ResultData;
import com.open.free.boot.web.model.UserInfoModel;
import com.open.free.boot.web.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/10 23:47
 * @Description
 */
@RestController
@RequestMapping("/api")
public class TestController extends BaseController {
    @Autowired
    private UserInfoService userInfoService;

    @GetMapping("/test")
    @JwtIgnore
    public ResultData test(){
        ResultData resultData = new ResultData();

        resultData.setCode(200);
        return resultData;
    }

    @PostMapping("/test3")
    @JwtIgnore
    public ResultData test3(@RequestParam String name, @RequestParam String id){
        System.out.println(name+"----"+id);
        ResultData resultData = new ResultData();
        resultData.setCode(200);
        return resultData;
    }
    @PostMapping("/test4")
    @JwtIgnore
    public ResultData test4(@RequestBody UserInfoModel user){
        ResultData resultData = new ResultData();
        userInfoService.insertUserInfo(user);
        resultData.setCode(200);
        return resultData;
    }

    @GetMapping("/test2")
    @JwtIgnore
    public ResultData test2(){
        ResultData resultData = new ResultData();
        resultData.setCode(200);
        resultData.setMessage("测试TOKEN");
        resultData.setData(userInfoService.getUserInfo(1L));
        return resultData;
    }
}
