package com.open.free.boot.web.service;

import com.open.free.boot.web.mapper.SysMenuModelMapper;
import com.open.free.boot.web.model.SysMenuModel;
import com.open.free.boot.web.utils.TreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/15 23:01
 * @Description
 */
@Service
public class MenuService {
    @Autowired
    private SysMenuModelMapper sysMenuModelMapper;

    /**
     * 获取系统所有菜单
     * @return
     */
    public List<Object>getSysmenuModelList(){
        //TODO 根据登陆者ID 获取当前登陆者具有哪些菜单
        List<SysMenuModel> sysMenuModels = sysMenuModelMapper.selectByExample(null);
        TreeUtil treeUtil = new TreeUtil();
        return treeUtil.treeMenu(sysMenuModels);
    }

}
