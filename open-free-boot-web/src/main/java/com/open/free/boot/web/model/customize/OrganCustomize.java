package com.open.free.boot.web.model.customize;

import com.open.free.boot.web.model.OrganOfficeModel;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/10/3 15:50
 * @Description
 */
public class OrganCustomize extends OrganOfficeModel {
    private String organName;

    public String getOrganName() {
        return organName;
    }

    public void setOrganName(String organName) {
        this.organName = organName;
    }
}
