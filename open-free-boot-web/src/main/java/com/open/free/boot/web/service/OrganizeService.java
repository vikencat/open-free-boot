package com.open.free.boot.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.open.free.boot.common.constant.Constants;
import com.open.free.boot.web.mapper.CompanyInfoModelMapper;
import com.open.free.boot.web.mapper.OrganOfficeModelMapper;
import com.open.free.boot.web.mapper.customize.OrganOfficeCustomizeMapper;
import com.open.free.boot.web.model.CompanyInfoModel;
import com.open.free.boot.web.model.CompanyInfoModelExample;
import com.open.free.boot.web.model.OrganOfficeModel;
import com.open.free.boot.web.model.OrganOfficeModelExample;
import com.open.free.boot.web.model.customize.OrganCustomize;
import com.open.free.boot.web.requestQuery.OrganRequest;
import com.open.free.boot.web.utils.CompanyMenuTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.event.ObjectChangeListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/28 21:21
 * @Description
 */
@Service
public class OrganizeService {
    @Autowired
    private CompanyInfoModelMapper companyInfoModelMapper;
    @Autowired
    private OrganOfficeModelMapper organOfficeModelMapper;
    @Autowired
    private OrganOfficeCustomizeMapper organOfficeCustomizeMapper;

    public void insertOrganize(OrganRequest organRequest){
        CompanyInfoModel companyInfoModel = new CompanyInfoModel();
        companyInfoModel.setParentId(organRequest.getParentId());
        companyInfoModel.setOrgType(Constants.TWO);
        companyInfoModel.setOrganName(organRequest.getOrganName());
        companyInfoModel.setOrganShortName(organRequest.getOrganName());
        companyInfoModelMapper.insertSelective(companyInfoModel);
        organRequest.setOrganId(companyInfoModel.getId());
        organRequest.setCreateTime(new Date());
        organOfficeModelMapper.insertSelective(organRequest);
    }

    public List<Object> getOrganizeTree() {
      List<CompanyInfoModel> infoModelList = companyInfoModelMapper.selectByExample(null);
      CompanyMenuTree companyMenuTree = new CompanyMenuTree();
      return companyMenuTree.treeMenu(infoModelList);
    }

    /**
     * 查询父节点下部门信息
     * @param organRequest
     * @return
     */
    public PageInfo<OrganCustomize> getOrganCustomizeByParentId(OrganRequest organRequest){
        Map<String,Object> params = new HashMap<>();
        params.put("parentId",organRequest.getParentId());
        PageInfo<OrganCustomize> list = PageHelper.startPage(organRequest.getPageNo(),organRequest.getPageSize())
                .doSelectPageInfo(()->{
            organOfficeCustomizeMapper.getOrganCustomizeByParentId(params);
        });
        return list;
    }

    /**
     * 修改组织机构信息
     * @param organRequest
     */
    public void updateOrganize(OrganRequest organRequest){
        CompanyInfoModel companyInfoModel = new CompanyInfoModel();
        companyInfoModel.setParentId(organRequest.getParentId());
        companyInfoModel.setOrgType(Constants.TWO);
        companyInfoModel.setOrganName(organRequest.getOrganName());
        companyInfoModel.setOrganShortName(organRequest.getOrganName());
        companyInfoModel.setId(organRequest.getOrganId());
        companyInfoModelMapper.updateByPrimaryKeySelective(companyInfoModel);

        organRequest.setOrganId(companyInfoModel.getId());
        organRequest.setCreateTime(new Date());
        organOfficeModelMapper.updateByPrimaryKeySelective(organRequest);
    }

    /**
     * 获取单个组织机构信息
     * @param id
     * @return
     */
    public OrganCustomize getOrganizeById(Long id) {

        Map<String,Object> params = new HashMap<>();
        params.put("id",id);
        return organOfficeCustomizeMapper.getOrganizeById(params);
    }

    /**
     * 删除组织机构信息
     * @param organId
     */
    public void deleteOrganizeById(Long organId,Long id){
        organOfficeModelMapper.deleteByPrimaryKey(id);
        companyInfoModelMapper.deleteByPrimaryKey(organId);
    }
}
