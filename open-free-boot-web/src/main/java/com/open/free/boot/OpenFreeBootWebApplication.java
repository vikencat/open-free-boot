package com.open.free.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.open.free.boot.web.mapper")
public class OpenFreeBootWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenFreeBootWebApplication.class, args);
    }

}
