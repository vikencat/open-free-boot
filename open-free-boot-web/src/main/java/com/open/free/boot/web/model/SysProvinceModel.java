package com.open.free.boot.web.model;

import java.io.Serializable;

public class SysProvinceModel implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_sys_province.id
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_sys_province.name
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_sys_province.province_code
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    private Integer provinceCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_sys_province.enable
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    private Integer enable;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table t_sys_province
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_sys_province.id
     *
     * @return the value of t_sys_province.id
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_sys_province.id
     *
     * @param id the value for t_sys_province.id
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_sys_province.name
     *
     * @return the value of t_sys_province.name
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_sys_province.name
     *
     * @param name the value for t_sys_province.name
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_sys_province.province_code
     *
     * @return the value of t_sys_province.province_code
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    public Integer getProvinceCode() {
        return provinceCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_sys_province.province_code
     *
     * @param provinceCode the value for t_sys_province.province_code
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    public void setProvinceCode(Integer provinceCode) {
        this.provinceCode = provinceCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_sys_province.enable
     *
     * @return the value of t_sys_province.enable
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    public Integer getEnable() {
        return enable;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_sys_province.enable
     *
     * @param enable the value for t_sys_province.enable
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_sys_province
     *
     * @mbggenerated Sat Sep 12 23:36:16 CST 2020
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", provinceCode=").append(provinceCode);
        sb.append(", enable=").append(enable);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}