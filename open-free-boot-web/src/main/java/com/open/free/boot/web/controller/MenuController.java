package com.open.free.boot.web.controller;

import com.open.free.boot.core.controller.BaseController;
import com.open.free.boot.core.result.ResultData;
import com.open.free.boot.web.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/15 22:04
 * @Description
 */
@RestController
@RequestMapping("/api")
public class MenuController extends BaseController {

    @Autowired
    private MenuService menuService;

    /**
     * 获取所有菜单
     * @return
     */
    @PostMapping("/getMenu")
    public ResultData getMenuList(){
        return resultSuccess(menuService.getSysmenuModelList());
    }
}
