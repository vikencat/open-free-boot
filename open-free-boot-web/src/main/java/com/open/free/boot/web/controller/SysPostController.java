package com.open.free.boot.web.controller;

import com.open.free.boot.core.controller.BaseController;
import com.open.free.boot.core.result.ResultData;
import com.open.free.boot.web.model.SysPost;
import com.open.free.boot.web.service.SysPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Liuzhiwei
 * @date 2020/09/28
 * @version 1.0
 */
@RestController
@RequestMapping("/api/sysPost")
public class SysPostController extends BaseController {

    @Autowired
    private SysPostService sysPostService;

    @PostMapping("/insert")
    public ResultData insert(@RequestBody SysPost request) {
        sysPostService.insert(request);
        return super.resultSuccess();
    }

    @PostMapping("/updateById")
    public ResultData updateById(SysPost request) {
        sysPostService.updateById(request);
        return super.resultSuccess();
    }

    @PostMapping("/removeById")
    public ResultData removeById(@RequestBody SysPost request) {
        sysPostService.removeById(request);
        return super.resultSuccess();
    }

    @GetMapping("/selectByQuery")
    public ResultData selectByQuery() {
        return super.resultSuccess(sysPostService.selectByQuery());
    }
}
