package com.open.free.boot.web.service;

import com.open.free.boot.common.constant.Constants;
import com.open.free.boot.common.lang.StringToolUtils;
import com.open.free.boot.web.mapper.CompanyInfoModelMapper;
import com.open.free.boot.web.model.CompanyInfoModel;
import com.open.free.boot.web.model.CompanyInfoModelExample;
import com.open.free.boot.web.utils.CompanyTreeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/22 16:33
 * @Description
 */
@Service
public class CompanyService {

    @Autowired
    private CompanyInfoModelMapper companyInfoModelMapper;

    /**
     * 保存企业信息
     * @param companyInfoModel
     */
    public void insertCompanyInof(CompanyInfoModel companyInfoModel){
        // 如果上级节点为空 则为 平级根节点
        if(StringToolUtils.isEmptyObj(companyInfoModel.getParentId())){
            companyInfoModel.setParentId(Constants.ZERRO);
        }
        companyInfoModel.setOrgType(Constants.ONE);
        companyInfoModelMapper.insertSelective(companyInfoModel);
    }

    /**
     * 修改企业信息
     * @param companyInfoModel
     */
    public void updateCompanyInfo(CompanyInfoModel companyInfoModel){
        companyInfoModelMapper.updateByPrimaryKeySelective(companyInfoModel);
    }

    /**
     * 删除企业信息 根据逐渐ID 删除
     * @param companyId
     * @return
     */
    public boolean deleteCompanyInfoById(Long companyId){

        // 判断当前ID 下是否有子公司
        CompanyInfoModelExample example =  new CompanyInfoModelExample();
        example.createCriteria().andParentIdEqualTo(companyId);
        List<CompanyInfoModel> companyInfoModels = companyInfoModelMapper.selectByExample(example);
        // 判断当前节点是否有子节点
        if(!companyInfoModels.isEmpty()){
            return false;
        }else{
            companyInfoModelMapper.deleteByPrimaryKey(companyId);
            return true;
        }
    }

    /**
     * 获取跟节点企业信息
     * @return
     */
    public List<CompanyInfoModel> getRootCompanyList(){
        CompanyInfoModelExample example = new CompanyInfoModelExample();
        example.createCriteria().andParentIdEqualTo(Constants.ZERRO).andOrgTypeEqualTo(Constants.ONE);
        return companyInfoModelMapper.selectByExample(example);
    }

    /**
     * 获取子组织机构节点
     * @param parentId
     * @return
     */
    public List<CompanyInfoModel> getCompanyListByParentId(Long parentId,Integer orgType){
        CompanyInfoModelExample example = new CompanyInfoModelExample();

        if(orgType!=null){
            example.createCriteria().andParentIdEqualTo(parentId).andOrgTypeEqualTo(orgType);
        }else{
            example.createCriteria().andParentIdEqualTo(parentId);
        }
        return companyInfoModelMapper.selectByExample(example);
    }


    /**
     * 获取跟节点企业信息
     * @return
     */
    public List<Object> getCompanyTreeList(String companyName){
        CompanyInfoModelExample example = new CompanyInfoModelExample();
        if(StringUtils.isBlank(companyName)){
            example.createCriteria().andOrgTypeEqualTo(Constants.ONE);
        }else{
            example.createCriteria().andOrgTypeEqualTo(Constants.ONE).andOrganNameLike("%"+companyName+"%");
        }
        List<CompanyInfoModel> modelList = companyInfoModelMapper.selectByExample(example);
        CompanyTreeUtils treeUtils = new CompanyTreeUtils();
        return treeUtils.treeMenu(modelList);
    }

    /**
     * 查询单个
     * @param id
     * @return
     */
    public CompanyInfoModel getCompanyInfoModelById(Long id){
        return companyInfoModelMapper.selectByPrimaryKey(id);
    }



}
