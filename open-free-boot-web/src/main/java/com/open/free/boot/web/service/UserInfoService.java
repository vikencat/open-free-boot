package com.open.free.boot.web.service;

import com.open.free.boot.web.mapper.UserInfoModelMapper;
import com.open.free.boot.web.model.UserInfoModel;
import com.open.free.boot.web.model.UserInfoModelExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/9 23:28
 * @Description
 */
@Service
public class UserInfoService {

    @Autowired
    private UserInfoModelMapper userInfoModelMapper;

    /**
     * 用户注册
     * @param model
     */
    public void insertUserInfo(UserInfoModel model){
        userInfoModelMapper.insertSelective(model);
    }

    /**
     * 查询用户是否注册
     * @param userName
     * @return
     */
    public List<UserInfoModel> getUserHasSignUp(String userName){
        UserInfoModelExample example = new UserInfoModelExample();
        example.createCriteria().andUserNameEqualTo(userName);
        return userInfoModelMapper.selectByExample(example);
    }
    /**
     * 根据用户ID 获取基本信息
     * @param id
     * @return
     */
    public UserInfoModel getUserInfo(Long id){
       return userInfoModelMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据用户名修改密码
     * @param userInfoModel
     */
    public void updateUserPassWord(UserInfoModel userInfoModel ){
        UserInfoModelExample example = new UserInfoModelExample();
        example.createCriteria().andUserNameEqualTo(userInfoModel.getUserName());
        userInfoModelMapper.updateByExampleSelective(userInfoModel,example);
    }

    /**
     * 用户登录
     * @param userName
     * @param passWord
     * @return
     */
    public List<UserInfoModel> getUserSignIn(String userName,String passWord){
        UserInfoModelExample example = new UserInfoModelExample();
        example.createCriteria().andUserNameEqualTo(userName).andUserPassEqualTo(passWord);
        return userInfoModelMapper.selectByExample(example);
    }
}
