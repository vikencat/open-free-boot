package com.open.free.boot.web.controller;

import com.github.pagehelper.PageInfo;
import com.open.free.boot.core.controller.BaseController;
import com.open.free.boot.core.result.ResultData;
import com.open.free.boot.core.result.ResultEnum;
import com.open.free.boot.web.model.OrganOfficeModel;
import com.open.free.boot.web.model.customize.OrganCustomize;
import com.open.free.boot.web.requestQuery.OrganRequest;
import com.open.free.boot.web.service.OrganizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/28 21:20
 * @Description
 */
@RestController
@RequestMapping("/api")
public class OrganizeController extends BaseController {
    @Autowired
    private OrganizeService organizeService;


    @PostMapping("/organ/insertOrganize")
    public ResultData insertOrganize(@RequestBody OrganRequest organRequest){
        organizeService.insertOrganize(organRequest);
        return super.resultSuccess();
    }

    /**
     * 组织机构左侧菜单树
     * @return
     */
    @PostMapping("/organ/getOrganizeTree")
    public ResultData getOrganizeTree(){
        return super.resultSuccess(organizeService.getOrganizeTree());
    }

    /**
     * 获取公司旗下子机构
     * @param organRequest
     * @return
     */
    @PostMapping("/organ/getOrgainzeSubList")
    public ResultData getOrgainzeSubList(@RequestBody OrganRequest organRequest){
        PageInfo<OrganCustomize> list = organizeService.getOrganCustomizeByParentId(organRequest);
        return super.resultSuccess(list);
    }

    /**
     * 修改组织信息
     * @param organRequest
     * @return
     */
    @PostMapping("/organ/updateOrganize")
    public ResultData updateOrganize(@RequestBody OrganRequest organRequest){
            organizeService.updateOrganize(organRequest);
        return super.resultSuccess();
    }

    /**
     * 查询单个组织机构信息
     * @param organRequest
     * @returngetOrganizeById
     */
    @PostMapping("/organ/getOrganizeById")
    public ResultData getOrganizeById(@RequestBody OrganRequest organRequest){
        return super.resultSuccess(organizeService.getOrganizeById(organRequest.getId()));
    }

    /**
     * 删除当前组织机构信息
     * @param organRequest
     * @return
     */
    @PostMapping("/organ/deleteOrganizeById")
    public ResultData deleteOrganizeById(@RequestBody OrganRequest organRequest) {
        organizeService.deleteOrganizeById(organRequest.getOrganId(),organRequest.getId());
        return super.resultSuccess();
    }
}
