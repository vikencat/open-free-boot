package com.open.free.boot.web.requestQuery;

import com.open.free.boot.core.entity.RequestPage;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/13 10:28
 * @Description
 */
public class AreaRequest extends RequestPage {
    private Integer id;
    private Integer provinceCode;
    private Integer cityCode;
    private Integer tableType;
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(Integer provinceCode) {
        this.provinceCode = provinceCode;
    }

    public Integer getCityCode() {
        return cityCode;
    }

    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public Integer getTableType() {
        return tableType;
    }

    public void setTableType(Integer tableType) {
        this.tableType = tableType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
