package com.open.free.boot.web.utils;

import com.open.free.boot.web.model.SysMenuModel;
import com.open.free.boot.web.model.customize.MenuModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/15 23:37
 * @Description
 */
public class TreeUtil {
    public static Map<String,Object> mapArray = new LinkedHashMap<String, Object>();

    public  List<SysMenuModel> menuCommon;
    public  List<Object> list = new ArrayList<Object>();

    public  List<Object> treeMenu(List<SysMenuModel> menu){
        menuCommon = menu;
        for (SysMenuModel treeNode : menu) {
            Map<String,Object> mapArr = new LinkedHashMap<String, Object>();
            if(treeNode.getParentId()==0L){
                setTreeMap(mapArr,treeNode);
                list.add(mapArr);
            }
        }
        return list;
    }

    public  List<?> menuChild(long id){
        List<Object> lists = new ArrayList<Object>();
        for(SysMenuModel a:menuCommon){
            Map<String,Object> childArray = new LinkedHashMap<String, Object>();
            if(a.getParentId() .equals(id)){
                setTreeMap(childArray,a);
                lists.add(childArray);
            }
        }
        return lists;
    }

    private   void setTreeMap(Map<String,Object> mapArr,SysMenuModel treeNode){
        mapArr.put("id", treeNode.getId());
        mapArr.put("text", treeNode.getMenuText());
        mapArr.put("parentId", treeNode.getParentId());
        mapArr.put("i18n",treeNode.getMenuI18n());
        mapArr.put("group",treeNode.getMenuGroup());
        mapArr.put("link",treeNode.getMenuLink());
        mapArr.put("icon",treeNode.getMenuIcon());

        List<?> childrens = menuChild(treeNode.getId());
        if(childrens.size()>0){
            mapArr.put("hasChild",true);
        }
        else{
            mapArr.put("hasChildren",false);
        }
        mapArr.put("children", menuChild(treeNode.getId()));
    }
}
