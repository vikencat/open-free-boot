package com.open.free.boot.web.requestQuery;

import com.open.free.boot.web.model.OrganOfficeModel;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/28 23:19
 * @Description
 */
public class OrganRequest extends OrganOfficeModel {
    private String organName;
    private Long parentId;

    private Integer pageSize;
    private Integer pageNo;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public String getOrganName() {
        return organName;
    }


    public void setOrganName(String organName) {
        this.organName = organName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
