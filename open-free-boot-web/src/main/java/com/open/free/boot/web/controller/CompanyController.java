package com.open.free.boot.web.controller;

import com.open.free.boot.core.controller.BaseController;
import com.open.free.boot.core.result.ResultData;
import com.open.free.boot.web.model.CompanyInfoModel;
import com.open.free.boot.web.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/22 15:17
 * @Description 公司管理功能
 */
@RestController
@RequestMapping("/api")
public class CompanyController extends BaseController {

    @Autowired
    private CompanyService companyService;

    /**
     * 保存企业信息
     * @param companyInfo
     * @return
     */
    @PostMapping("/organ/insertCompany")
    public ResultData insertCompany(@RequestBody CompanyInfoModel companyInfo){
        companyService.insertCompanyInof(companyInfo);
        return super.resultSuccess();
    }

    /**
     * 修改企业信息
     * @param companyInfo
     * @return
     */
    @PostMapping("/organ/updateCompany")
    public ResultData updateCompany(@RequestBody CompanyInfoModel companyInfo){
        companyService.updateCompanyInfo(companyInfo);
        return super.resultSuccess();
    }

    /**
     * 获取企业跟节点信息
     * @return
     */
    @PostMapping("/organ/getCompanyList")
    public ResultData getCompanyList(){
        return super.resultSuccess(companyService.getRootCompanyList());
    }

    /**
     * 获取企业跟节点信息
     * @return
     */
    @PostMapping("/organ/getCompanyListByParentId")
    public ResultData getCompanyListByParentId(@RequestBody CompanyInfoModel companyInfo){
        return super.resultSuccess(companyService.getCompanyListByParentId(companyInfo.getParentId(),companyInfo.getOrgType()));
    }

    /**
     * 获取所有企业树形表格展示
     * @return
     */
    @PostMapping("/organ/getCompanyTreeList")
    public ResultData getCompanyTreeList(@RequestBody CompanyInfoModel companyInfo){
        return super.resultSuccess(companyService.getCompanyTreeList(companyInfo.getOrganName()));
    }

    /**
     * 查询单个数据信息
     * @param companyInfo
     * @return
     */
    @PostMapping("/organ/getCompanyInfoById")
    public ResultData getCompanyInfoById(@RequestBody CompanyInfoModel companyInfo){
        return super.resultSuccess(companyService.getCompanyInfoModelById(companyInfo.getId()));
    }

    /**
     * 删除子节点
     * @param companyInfo
     * @return
     */
    @PostMapping("/organ/deleteCompanyInfoById")
    public ResultData deleteCompanyInfoById(@RequestBody CompanyInfoModel companyInfo){
        return super.resultSuccess(companyService.deleteCompanyInfoById(companyInfo.getId()));
    }
}
