package com.open.free.boot.web.utils;

import com.open.free.boot.web.model.CompanyInfoModel;
import com.open.free.boot.web.model.SysMenuModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/24 14:05
 * @Description
 */
public class CompanyTreeUtils {
    public static Map<String,Object> mapArray = new LinkedHashMap<String, Object>();

    public List<CompanyInfoModel> menuCommon;
    public  List<Object> list = new ArrayList<Object>();

    public  List<Object> treeMenu(List<CompanyInfoModel> menu){
        menuCommon = menu;
        for (CompanyInfoModel treeNode : menu) {
            Map<String,Object> mapArr = new LinkedHashMap<String, Object>();
            if(treeNode.getParentId()==0L){
                setTreeMap(mapArr,treeNode);
                list.add(mapArr);
            }
        }
        return list;
    }

    public  List<?> menuChild(long id){
        List<Object> lists = new ArrayList<Object>();
        for(CompanyInfoModel a:menuCommon){
            Map<String,Object> childArray = new LinkedHashMap<String, Object>();
            if(a.getParentId() .equals(id)){
                setTreeMap(childArray,a);
                lists.add(childArray);
            }
        }
        return lists;
    }

    private   void setTreeMap(Map<String,Object> mapArr,CompanyInfoModel treeNode){
        mapArr.put("key", treeNode.getId());
        mapArr.put("name", treeNode.getOrganName());
        mapArr.put("shortName", treeNode.getOrganShortName());
        List<?> childrens = menuChild(treeNode.getId());
        if(childrens.size()>0){
            mapArr.put("hasChild",true);
        }
        else{
            mapArr.put("hasChildren",false);
        }
        mapArr.put("children", menuChild(treeNode.getId()));
    }
}
