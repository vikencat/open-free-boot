package com.open.free.boot.web.controller;

import com.github.pagehelper.PageInfo;
import com.open.free.boot.core.controller.BaseController;
import com.open.free.boot.core.result.ResultData;
import com.open.free.boot.web.model.SysAreaModel;
import com.open.free.boot.web.model.SysCityModel;
import com.open.free.boot.web.model.SysProvinceModel;
import com.open.free.boot.web.requestQuery.AreaRequest;
import com.open.free.boot.web.service.SysAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/12 22:20
 * @Description 行政区管理
 */
@RestController
@RequestMapping("/api")
public class SysAreaController extends BaseController {

    @Autowired
    private SysAreaService sysAreaService;

    /**
     * 获取所有省份列表
     * @param areaRequest
     * @return
     */
    @PostMapping("/getProvinceList")
    public ResultData getSysProvinceModelList(@RequestBody AreaRequest areaRequest){
        PageInfo<SysProvinceModel> list = sysAreaService.getSysProvinceModelList(areaRequest.getPageNo(),areaRequest.getPageSize());
        return super.resultSuccess(list);
    }

    /**
     * 获取城市
     * @param areaRequest
     * @return
     */
    @PostMapping("/getCityList")
    public ResultData getSysCityModelMapperList(@RequestBody AreaRequest areaRequest){
        List<SysCityModel>  list =sysAreaService.getSysCityModelMapperList(areaRequest.getProvinceCode());
        return super.resultSuccess(list);
    }

    /**
     * 获取区县
     * @param areaRequest
     * @return
     */
    @PostMapping("/getAreaList")
    public ResultData getSysAreaModelMapperList(@RequestBody AreaRequest areaRequest){
        List<SysAreaModel>  list =sysAreaService.getSysAreaModelMapperList(areaRequest.getCityCode());
        return super.resultSuccess(list);
    }

    /**
     * 修改停用 启用状态
     * @param areaRequest
     * @return
     */
    @PostMapping("/updateEnable")
    public ResultData unEnableStatus(@RequestBody AreaRequest areaRequest){

        sysAreaService.unEnableStatus(areaRequest.getId(),areaRequest.getTableType(),areaRequest.getStatus());

        return super.resultSuccess();
    }

    /**
     * 删除城所信息
     * @param areaRequest
     * @return
     */
    @PostMapping("/deleteArea")
    public ResultData deleteArea(@RequestBody AreaRequest areaRequest){
        sysAreaService.deleteArea(areaRequest.getId(),areaRequest.getTableType());
        return super.resultSuccess();
    }
}
