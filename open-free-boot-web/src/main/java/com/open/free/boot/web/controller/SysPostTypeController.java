package com.open.free.boot.web.controller;

import com.open.free.boot.core.controller.BaseController;
import com.open.free.boot.core.result.ResultData;
import com.open.free.boot.web.service.SysPostTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Liuzhiwei
 * @date 2020/09/28
 * @version 1.0
 */
@RestController
@RequestMapping("/api/sysPostType")
public class SysPostTypeController extends BaseController {

    @Autowired
    private SysPostTypeService sysPostTypeService;

    @PostMapping("/getEnableSysPostTypeList")
    public ResultData getEnableSysPostTypeList() {
        return super.resultSuccess(sysPostTypeService.getEnableSysPostTypeList());
    }
}
