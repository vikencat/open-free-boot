package com.open.free.boot.common.lang;



import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.StringUtils;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

/**
 * @author Faye.Wang
 * @version 1.0
 * @date 2020/9/11 20:34
 * @Description
 */
public class StringToolUtils {

    private static final String SYMBOLS = "0123456789";
    private static final Random RANDOM = new SecureRandom();

    /**
     * 获取长度为 6 的随机数字
     * @return 随机数字
     */
    public static String generatorRollNum() {
        char[] nonceChars = new char[6];

        for (int index = 0; index < nonceChars.length; ++index) {
            nonceChars[index] = SYMBOLS.charAt(RANDOM.nextInt(SYMBOLS.length()));
        }

        return new String(nonceChars);
    }

    /**
     * 判断对象是否为空
     * @param obj
     * @return
     */
    public static boolean isEmptyObj(Object obj){
        return StringUtils.isEmpty(obj);
    }

    /**
     * SHA512 加密
     * @param pass 密码
     * @param salt 秘钥
     * @return
     */
    public static String digestPassword(String pass,String salt){
        String password = pass+salt;
        MessageDigest messageDigest = DigestUtils.getSha512Digest();
        byte[] bytes = messageDigest.digest(password.getBytes());
        return Hex.encodeHexString(bytes);
    }

    public static String getGeneratorUUID(){
        return  UUID.randomUUID().toString().replaceAll("-", "");
    }
}
